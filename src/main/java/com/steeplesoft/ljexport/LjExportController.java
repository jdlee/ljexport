/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.ljexport;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;

/**
 *
 * @author jdlee
 */
public class LjExportController implements Initializable {

    @FXML
    BorderPane rootPane;
    
    @FXML
    TextField userName;

    @FXML
    PasswordField password;

    @FXML
    TextField year;

    @FXML
    ComboBox month;
    
    @FXML
    Button exportButton;

    @FXML
    TextField outputDirectory;
    
    @FXML
    private Label statusLine;
    
    private static final String URL_LOGIN = "https://www.livejournal.com/login.bml";
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();
    private String cookies;
    private final List<String> wantedCookies = Arrays.asList(
            "ljident", "ljuniq", "xtvrn", "xtan", "xtant", "langpref",
            "ljmastersession", "ljloggedin", "BMLschemepref", "ljsession"
    );

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList months = FXCollections.observableArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        month.setItems(months);

    }

    @FXML
    private void handleButtonAction(ActionEvent event) {
        try {
            LocalDate now = LocalDate.now();
            int currYear = Integer.parseInt(year.getText());
            int currMonth = Integer.parseInt(month.getValue().toString());
            login();
            File dir = new File(outputDirectory.getText());
            while (true) {
                if ((currYear == now.getYear()) && (currMonth > now.getMonthValue())) {
                    break;
                }
                updateStatusLine("Exporting " + year + "/" + month);
                String month = exportMonth(currYear, currMonth);
                outputToFile(dir, currYear, currMonth, month);
                currMonth++;
                if (currMonth > 12) {
                    currMonth = 1;
                    currYear++;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LjExportController.class.getName()).log(Level.SEVERE, null, ex);
        }
        updateStatusLine("Done.");
    }
    
    private void outputToFile(File dir, int year, int month, String content) throws IOException {
        String fileName = userName.getText() + "-export-" + year + "-" + pad(month,2) + ".xml";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(dir, fileName)))) {
            writer.write(content);
        }
    }

    private void handleDirectorySelection() {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Select Output Directory");
        File dir = chooser.showDialog(rootPane.getScene().getWindow());
        if (dir != null) {
            outputDirectory.setText(dir.getAbsolutePath());
            exportButton.setDisable(false);
        }
    }
    
    @FXML
    private void outputDirectoryClicked(MouseEvent event) {
        handleDirectorySelection();
    }
    
    @FXML
    private void outputDirectoryKeyPress(KeyEvent event) {
        if ((event.getCode() == KeyCode.SPACE) || (event.getCode() == KeyCode.ENTER)) {
            handleDirectorySelection();
        }
    }

    private String login() throws IOException {
        updateStatusLine("Logging in...");
        FormEncodingBuilder builder = new FormEncodingBuilder();
        RequestBody body = builder.add("user", userName.getText())
                .add("password", password.getText())
                .build();
        Request request = new Request.Builder()
                .url(URL_LOGIN)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        processCookies(response.headers("Set-Cookie"));
        updateStatusLine("Logged in.");
        return response.body().string();
    }

    protected void updateStatusLine(String message) {
        Platform.runLater(() -> statusLine.setText(message));
    }

    private void processCookies(List<String> list) {
        StringBuilder sb = new StringBuilder();
        for (String cookie : list) {
            int index = cookie.indexOf("=");
            String cookieName = cookie.substring(0, index);
            if (wantedCookies.contains(cookieName)) {
                sb.append(cookie.substring(0, cookie.indexOf(";") + 1)).append(" ");
            }
        }
        cookies = sb.toString();
    }

    private String exportMonth(int year, int month) throws IOException {
        FormEncodingBuilder builder = new FormEncodingBuilder();
        RequestBody body = builder
                .add("encid", "2")
                .add("field_allowmask", "on")
                .add("field_currents", "on")
                .add("field_event", "on")
                .add("field_eventtime", "on")
                .add("field_itemid", "on")
                .add("field_logtime", "on")
                .add("field_security", "on")
                .add("field_subject", "on")
                .add("format", "xml")
                .add("month", "" + month)
                .add("what", "journal")
                .add("year", "" + year)
                .build();
        Request request = new Request.Builder()
                .url("http://www.livejournal.com/export_do.bml?authas=" + userName.getText())
                .addHeader("Cookie", cookies)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
    
    private String pad(int number, int width) {
        final String text = "0000" + number;
        return text.substring(text.length()-width);
    }
}
